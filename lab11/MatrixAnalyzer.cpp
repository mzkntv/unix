#include "MatrixAnalyzer.h"
#include <climits>

MatrixAnalyzer::MatrixAnalyzer(MatrixReader &r)
    : reader(r)
{
    reader.read();
}

MatrixAnalyzer::~MatrixAnalyzer()
{
    delete &reader;
}

MatrixAnalyzer *(MatrixAnalyzer::fromFile)(const char *filename)
{
    FileMatrixReader *f = new FileMatrixReader(filename);
    FileMatrixReader &ref = *f;

    return new MatrixAnalyzer(ref);
}

MatrixAnalyzer *(MatrixAnalyzer::fromFake)()
{
    FakeMatrixReader *r = new FakeMatrixReader;
    FakeMatrixReader &ref = *r;

    return new MatrixAnalyzer(ref);
}

int MatrixAnalyzer::multiplyRowElements(const int &rowIndex)
{
    int result = reader.matrix[rowIndex][0];

    for (int i = 1; i < reader.matrix.colSize(); i++)
    {
        result = result * reader.matrix[rowIndex][i];
    }

    return result;
}

int MatrixAnalyzer::findRowIndexWithMaximumMultiplicationOfElements()
{
    int maxRowIndex = 0;
    int maxRowValue = INT_MIN;
    int currentRowMultiplication;

    for (int rowIndex = 0; rowIndex < reader.matrix.rowSize(); rowIndex++)
    {
        currentRowMultiplication = multiplyRowElements(rowIndex);
        if (currentRowMultiplication > maxRowValue)
        {
            maxRowIndex = rowIndex;
            maxRowValue = currentRowMultiplication;
        }
    }

    return maxRowIndex;
}

int MatrixAnalyzer::findRowIndexWithMinimumMultiplicationOfElements()
{
    int minRowIndex = 0;
    int minRowValue = INT_MAX;
    int currentRowMultiplication;

    for (int rowIndex = 0; rowIndex < reader.matrix.rowSize(); rowIndex++)
    {
        currentRowMultiplication = multiplyRowElements(rowIndex);
        if (currentRowMultiplication < minRowValue)
        {
            minRowIndex = rowIndex;
            minRowValue = currentRowMultiplication;
        }
    }

    return minRowIndex;
}

void MatrixAnalyzer::showMatrix()
{
    reader.matrix.print(std::cout);
}
