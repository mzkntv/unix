#pragma once

#include "Matrix.h"
#include "MatrixReader.h"

class MatrixAnalyzer
{
private:
    MatrixReader &reader;

public:
    MatrixAnalyzer(MatrixReader &);
    ~MatrixAnalyzer();

    static MatrixAnalyzer *fromFile(const char *);
    static MatrixAnalyzer *fromFake();

    int findRowIndexWithMaximumMultiplicationOfElements();
    int findRowIndexWithMinimumMultiplicationOfElements();
    int multiplyRowElements(const int &);
    void showMatrix();
};
