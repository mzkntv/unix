#pragma once
#include <vector>
#include <iostream>

class Matrix
{
private:
    std::vector<std::vector<int>> matrix = {{0}};
    int numberOfRows = 1;
    int numberOfColumns = 1;

public:
    Matrix();
    Matrix(std::vector<std::vector<int>> m);

    std::vector<int> &operator[](int);

    int rowSize() const;
    int colSize() const;
    void print(std::ostream &stream, const char separator = ' ');
};
