#pragma once

#include <fstream>

#include "Matrix.h"

class MatrixReader
{
private:
public:
    Matrix matrix;
    virtual void read() = 0;
    virtual ~MatrixReader();
};

class FakeMatrixReader : public MatrixReader
{
public:
    void read() override;
};

class FileMatrixReader : public MatrixReader
{
private:
    std::ifstream fin;
    const char *filename;

public:
    FileMatrixReader(const char *);
    void read();
};
