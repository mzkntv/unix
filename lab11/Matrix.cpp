#include "Matrix.h"

Matrix::Matrix()
{
}

Matrix::Matrix(std::vector<std::vector<int>> m)
    : matrix(m)
{
    numberOfRows = m.size();
    numberOfColumns = m[0].size();
}

std::vector<int> &Matrix::operator[](int rowIndex)
{
    if (rowIndex < 0 && rowIndex >= numberOfRows)
    {
        std::clog << "Matrix bound violation" << std::endl;
    }
    return matrix[rowIndex];
}

void Matrix::print(std::ostream &stream, const char separator)
{
    for (int row = 0; row < Matrix::matrix.size(); row++)
    {
        for (int column = 0; column < Matrix::matrix[row].size(); column++)
        {
            stream << Matrix::matrix[row][column] << separator;
        }
        stream << std::endl;
    }
}

int Matrix::rowSize() const
{
    return this->numberOfRows;
}

int Matrix::colSize() const
{
    return this->numberOfColumns;
}
