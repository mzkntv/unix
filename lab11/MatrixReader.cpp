#include <ctime>
#include <regex>

#include "MatrixReader.h"

MatrixReader::~MatrixReader()
{
}

FileMatrixReader::FileMatrixReader(const char *f)
    : filename(f)
{
}

void FileMatrixReader::read()
{
    std::vector<std::vector<int>> m;
    int cell = 0;

    fin.open(filename);

    if (!fin.good())
    {
        std::clog << "Файл с именем " << filename << " не найден" << std::endl;
        exit(-1);
    }

    while (!fin.eof())
    {
        std::string s;
        std::vector<int> row;
        size_t pos = 0;

        std::getline(fin, s);
        s = s + " ";

        while ((pos = s.find(" ") != std::string::npos))
        {
            cell = std::stoi(s.substr(0, pos));
            row.push_back(cell);
            s.erase(0, pos + 1);
        }
        m.push_back(row);
    }
    Matrix matrixFromFile(m);
    matrix = matrixFromFile;
}

void FakeMatrixReader::read()
{
    long ltime = std::time(NULL);
    const unsigned int DEFAULT_DIMENSION = 4;
    std::vector<std::vector<int>> randomSquareMatrix;

    // Generate new seed depends on current time
    std::srand((unsigned int)ltime / 2);

    // Generate random square matrix
    for (int row = 0; row < DEFAULT_DIMENSION; row++)
    {
        std::vector<int> r = {};
        for (int column = 0; column < DEFAULT_DIMENSION; column++)
        {
            r.push_back(std::rand() % 10);
        }
        randomSquareMatrix.push_back(r);
    }

    Matrix randomMatrix(randomSquareMatrix);

    matrix = randomMatrix;
}
