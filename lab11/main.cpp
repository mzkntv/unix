#include <iostream>
#include <unistd.h>

#include "MatrixAnalyzer.h"

int main(int argc, char **argv)
{
    int i;
    MatrixAnalyzer *analyzer = NULL;

    while ((i = getopt(argc, argv, "f:")) != -1)
    {
        if ((char)i == 'f')
        {
            analyzer = MatrixAnalyzer::fromFile(optarg);
        }
    }

    if (!(bool)analyzer)
    {
        analyzer = MatrixAnalyzer::fromFake();
    }

    int rowIndex = analyzer->findRowIndexWithMaximumMultiplicationOfElements();
    
    analyzer->showMatrix();

    std::cout << "Наибольшее произведение в строке " << rowIndex << std::endl;
    std::cout << "Произведение элементов в этой строке равно " << analyzer->multiplyRowElements(rowIndex) << std::endl;

    rowIndex = analyzer->findRowIndexWithMinimumMultiplicationOfElements();

    std::cout << "Наименьшее произведение в строке " << rowIndex << std::endl;
    std::cout << "Произведение элементов в этой строке равно " << analyzer->multiplyRowElements(rowIndex) << std::endl;

    delete analyzer;

    return 0;
}
