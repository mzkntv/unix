#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unistd.h>
#include <ctime>

class IReadable
{
public:
    virtual std::vector<int> read() = 0;
};

class FileReader : public IReadable
{
private:
    std::string filename;

public:
    FileReader(std::string f)
        : filename(f)
    {
    }
    std::vector<int> read() override
    {
        std::ifstream fin(filename);
        std::vector<int> v;
        int currentNumber = 0;

        if (!fin.good())
        {
            throw "File not found";
        }

        while (fin >> currentNumber)
        {
            v.push_back(currentNumber);
        }

        return v;
    }
};

class FakeReader : public IReadable
{
public:
    std::vector<int> read() override
    {
        long ltime;

        std::cout << "Reading from fake reader" << std::endl;

        // Initialize random seed by current time
        ltime = time(NULL);
        std::srand((unsigned int)ltime);

        // Array with 3 random elements
        std::vector<int> v(3);
        std::generate(v.begin(), v.end(), std::rand);

        return v;
    }
};

class Application
{
private:
    std::vector<int> numberSequence;

    int findMinivalValueIndex()
    {
        int minimalValueIndex = 0;

        for (int i = 1; i < numberSequence.size(); i++)
        {
            if (numberSequence[minimalValueIndex] >= numberSequence[i])
            {
                minimalValueIndex = i;
            }
        }

        return minimalValueIndex;
    }
    int calculateSequenceAverage()
    {
        int average = 0;
        int i = 0;

        while (i < numberSequence.size())
        {
            average = average + numberSequence[i];
            i++;
        }
        average = (int)(average / i);

        return average;
    }

public:
    IReadable &reader;
    Application(IReadable &r)
        : reader(r)
    {
    }

    void load()
    {
        numberSequence = reader.read();

        if (numberSequence.empty())
        {
            throw "Empty number sequence";
        }
    }

    void doMagic()
    {
        int minimalValueIndex = findMinivalValueIndex();
        int average = calculateSequenceAverage();

        numberSequence[minimalValueIndex] = average;
    }

    void printSequence()
    {
        for (int i = 0; i < numberSequence.size(); i++)
        {
            std::cout << numberSequence[i] << " ";
        }
        std::cout << std::endl;
    }
};

int main(int argc, char **argv)
{
    int i;
    IReadable *ptr = NULL;
    while ((i = getopt(argc, argv, "f:")) != -1)
    {
        if ((char)i == 'f')
        {
            ptr = new FileReader(optarg);
        }
    }

    if (!(bool)ptr)
    {
        ptr = new FakeReader;
    }

    IReadable &ref = *ptr;

    Application app(ref);
    try
    {
        app.load();
    }
    catch (const char *e)
    {
        std::cerr << e << std::endl;
        return -1;
    }

    // Before mutation
    app.printSequence();

    // Mutating sequnce
    app.doMagic();

    // After mutation
    app.printSequence();

    delete &ref;

    return 0;
}
