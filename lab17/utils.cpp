#include "utils.h"
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <fstream>
 
namespace utils
{
    void Divider()
    {
        std::cout << "=================================" << std::endl;
    }
    
    int Random::GetRandomNumber(int min, int max)
    {
        return min + rand() % (max - min + 1);
    }
    
    std::string Random::getName()
    {
        int index = this->GetRandomNumber(0, 9);
 
        return this->names[index];
    }    
 
    std::string Random::getSurname()
    {
        int index = this->GetRandomNumber(0, 9);
 
        return this->surnames[index];
    }
}
