#include <vector>
#include <string>
 
namespace utils
{
    void Divider();
    
    class Random
    {
        public:
            int GetRandomNumber(int min, int max);
            
            std::string getName();
 
            std::string getSurname();
 
        private:
            std::vector<std::string> names
            {
                "Bill",
                "Tom",
                "Dmitry",
                "Alex",
                "Vlad",
                "Nikita",
                "Jack",
                "Joe",
                "Ann",
                "Daria"
            };
 
            std::vector<std::string> surnames
            {
                "Smith",
                "Jones",
                "Moore",
                "Jackson",
                "Wood",
                "Harris",
                "Taylor",
                "Cooper",
                "Anderson",
                "Wilson"
            };
    };
}
