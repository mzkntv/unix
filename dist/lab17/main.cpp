#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

class ProductPosition
{
private:
    std::string title;
    unsigned int shelf_life;
    unsigned int cost;

    float calculateCostByShelfLife()
    {
        if (shelf_life >= 10)
        {
            return (float)cost / 2;
        }
        if (shelf_life > 6 && shelf_life < 10)
        {
            return (float)cost / 1.5;
        }
        return cost;
    }

public:
    ProductPosition(std::string t, unsigned int s, unsigned int c)
        : title(t), shelf_life(s), cost(c)
    {
    }

    void getProductOverview()
    {
        std::cout << "|" << std::setw(20) << title
                  << "|" << std::setw(20) << shelf_life
                  << "|" << std::setw(20) << cost
                  << "|" << std::setw(20) << calculateCostByShelfLife()
                  << "|" << std::endl;
        std::cout << "--------------------------------------------------------------------------------" << std::endl;
    }
};

std::vector<ProductPosition> getPositionsFromFile(std::string fileName)
{
    std::vector<ProductPosition> positions;
    std::ifstream fin(fileName);
    std::string rawString;
    std::string delimiter = ",";

    if (!fin.good())
    {
        std::clog << "Файл с именем " << fileName << " не найден" << std::endl;
        exit(-1);
    }

    while (!fin.eof())
    {
        std::vector<std::string> parsedString;
        size_t pos = 0;

        std::getline(fin, rawString);
        rawString = rawString + delimiter;
        while ((pos = rawString.find(delimiter)) != std::string::npos)
        {
            parsedString.push_back(rawString.substr(0, pos));
            rawString.erase(0, pos + delimiter.length());
        }
        positions.push_back(ProductPosition(parsedString[0], std::stoi(parsedString[1]), std::stoi(parsedString[2])));
    }

    return positions;
}

std::string gen_random(const int len)
{
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    std::string tmp_s;
    tmp_s.reserve(len);

    for (int i = 0; i < len; ++i)
    {
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return tmp_s;
}

ProductPosition generateRandomPosition()
{
    return ProductPosition(gen_random(15), std::rand() % 20 + 1, std::rand() % 1000 + 1);
}

int main(int argc, char **argv)
{
    int i;
    std::vector<ProductPosition> positions;

    // Проверяем, нужно ли брать данные из файла или генерим рандомный
    while ((i = getopt(argc, argv, "f:")) != -1)
    {
        if ((char)i == 'f')
        {
            positions = getPositionsFromFile(optarg);
        }
    }

    if (!(bool)positions.size())
    {
        std::srand((unsigned int)time(NULL));
        for (int i = 0; i < 10; i++)
        {
            positions.push_back(generateRandomPosition());
        }
    }
    std::cout << "--------------------------------------------------------------------------------" << std::endl;
    std::cout << "|" << std::setw(20) << "Nazvanie"
              << "|" << std::setw(20) << "Srok hraneniya"
              << "|" << std::setw(20) << "Tsena"
              << "|" << std::setw(20) << "Tsena posle utsenki"
              << "|" << std::endl;

    for (int i = 0; i < positions.size(); i++)
    {
        positions[i].getProductOverview();
    }

    return 0;
}
