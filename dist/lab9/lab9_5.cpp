#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <algorithm>
#include <string>
#include <unistd.h>

std::vector<int> generateRandomArray()
{
    std::srand((unsigned int)time(NULL));

    std::vector<int> v(5);
    std::generate(v.begin(), v.end(), std::rand);

    return v;
}

std::vector<int> getArrayFromFile(std::string fileName)
{
    std::ifstream fin(fileName);
    std::vector<int> v;
    int currentNumber = 0;

    if (!fin.good())
    {
        throw "File not found";
    }

    while (fin >> currentNumber)
    {
        v.push_back(currentNumber);
    }

    return v;
}

int main(int argc, char **argv)
{
    int i;
    std::vector<int> arr;
    std::vector<int> outArr;

    // Проверяем, нужно ли брать данные из файла или генерим рандомный
    while ((i = getopt(argc, argv, "f:")) != -1)
    {
        if ((char)i == 'f')
        {
            arr = getArrayFromFile(optarg);
        }
    }

    if (!arr.size())
    {
        arr = generateRandomArray();
    }

    for (int i = 0; i < arr.size(); i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;

    for (int i = 0; i < arr.size(); i++)
    {
        if (i % 2 != 0)
        {
            outArr.push_back(arr[i]);
        }
    }

    for (int i = 0; i < outArr.size(); i++)
    {
        std::cout << outArr[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
