#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <algorithm>
#include <string>
#include <unistd.h>

std::vector<std::vector<int>> generateRandomMatrix()
{
    std::srand((unsigned int)time(NULL));
    std::vector<std::vector<int>> matrix(5);

    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            matrix[i].push_back(0);
        }
    }

    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            std::generate(matrix[i].begin(), matrix[i].end(), std::rand);
        }
    }
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            matrix[i][j] = matrix[i][j] % 10 + 1;
        }
    }

    return matrix;
}

std::vector<std::vector<int>> getMatrixFromFile(std::string fileName)
{
    std::vector<std::vector<int>> m;
    std::ifstream fin;
    int cell = 0;

    fin.open(fileName);

    if (!fin.good())
    {
        std::clog << "Файл с именем " << fileName << " не найден" << std::endl;
        exit(-1);
    }

    while (!fin.eof())
    {
        std::string s;
        std::vector<int> row;
        size_t pos = 0;

        std::getline(fin, s);
        s = s + " ";

        while ((pos = s.find(" ") != std::string::npos))
        {
            cell = std::stoi(s.substr(0, pos));
            row.push_back(cell);
            s.erase(0, pos + 1);
        }
        m.push_back(row);
    }

    return m;
}

int main(int argc, char **argv)
{
    int k;
    int i;
    long kMultiplicationResult = 1;
    int kCounter = 0;
    std::vector<std::vector<int>> matrix;

    std::cout << "Введите k: ";
    std::cin >> k;

    // Проверяем, нужно ли брать данные из файла или генерим рандомный
    while ((i = getopt(argc, argv, "f:")) != -1)
    {
        if ((char)i == 'f')
        {
            matrix = getMatrixFromFile(optarg);
        }
    }

    if (!(bool)matrix.size())
    {
        matrix = generateRandomMatrix();
    }

    for (int row = 0; row < matrix.size(); row++)
    {
        for (int column = 0; column < matrix[row].size(); column++)
        {
            if (matrix[row][column] % k == 0)
            {
                kCounter++;
                kMultiplicationResult = kMultiplicationResult * matrix[row][column];
            }
        }
    }

    for (int row = 0; row < matrix.size(); row++)
    {
        for (int column = 0; column < matrix[row].size(); column++)
        {
            std::cout << matrix[row][column] << " ";
        }
        std::cout << std::endl;
    }

    if (kCounter)
    {
        std::cout << "Количество чисел, кратных " << k << ": " << kCounter << std::endl;
        std::cout << "Произведение чисел равно " << kMultiplicationResult << std::endl;
    }
    else
    {
        std::cout << "Не найдено чисел, кратных " << k << std::endl;
    }

    return 0;
}
