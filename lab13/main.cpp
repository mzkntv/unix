#include <iostream>
#include <map>

int main()
{
    std::string analyzingText;
    std::map<char, unsigned int> charFrequency = {};

    std::cin >> analyzingText;
    for (int i = 0; i < analyzingText.length(); i++)
    {
        if (charFrequency.find(analyzingText[i]) == charFrequency.end())
        {
            charFrequency.insert(std::pair<char, unsigned int>(analyzingText[i], 1));
        }
        else
        {
            charFrequency[analyzingText[i]] = charFrequency[analyzingText[i]] + 1;
        }
    }

    std::map<char, unsigned int>::iterator it = charFrequency.begin();

    for (int i = 0; it != charFrequency.end(); it++, i++)
    {
        float frequencePercent = (float)it->second / analyzingText.size() * 100;
        std::cout << "\"" << it->first << "\""
                  << ". Встречено " << it->second << " раз. " << frequencePercent << "%" << std::endl;
    }

    return 0;
}
