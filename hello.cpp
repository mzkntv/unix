#include <iostream>
#include <fstream>

using namespace std;

const int MAXBUFSIZE = 100;

class IReader
{
protected:
    int data[MAXBUFSIZE];

public:
    virtual void read() = 0;
    virtual ~IReader(){};
};

class FakeReader : public IReader
{
public:
    FakeReader() {}
    void read() override
    {
        cout << "Reading from random generator" << endl;
        for (int i = 0; i < 5; i++)
        {
            data[i] = rand();
        }
    }
};

class FileReader : public IReader
{
private:
    ifstream fin;

public:
    FileReader(string filename)
    {
        fin.open(filename);
        if (!fin)
        {
            cout << "File not found" << endl;
        }
    }
    void read() override
    {
        cout << "Reading from file " << endl;
    }
};

class SequenceHandler
{
private:
    int i = 0;

public:
    SequenceHandler(IReader &reader)
    {
        reader.read();
    }
    void calculate()
    {
        cout << "Do some stuff" << endl;
    }
};

int main()
{
    FakeReader reader;
    SequenceHandler s(reader);
    s.calculate();

    return 0;
}
